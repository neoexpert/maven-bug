package demo;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.*;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Set;

@SupportedAnnotationTypes("demo.DBConstructor")
@SupportedSourceVersion(SourceVersion.RELEASE_9)
public class AnnotationProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        for(TypeElement annotation : annotations) {
            Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(annotation);
            for(Element element : annotatedElements) {
                ElementKind kind = element.getKind();
                if(kind != ElementKind.CONSTRUCTOR) {
                    processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "@DBConstructor should annotate only constructors", element);
                    continue;
                }
                try {
                    writeBuilder((ExecutableElement) element);
                } catch (IOException e) {
                    processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "IOException occurred: ", element);
                    continue;
                }
            }
        }
        return true;
    }

    private void writeBuilder(ExecutableElement element) throws IOException {
        Element clazz = element.getEnclosingElement();
        Set<Modifier> modifiers = clazz.getModifiers();
        final String clazzName = clazz.getSimpleName().toString();
        if(!modifiers.contains(Modifier.PUBLIC)) {
            processingEnv.getMessager()
                    .printMessage(Diagnostic.Kind.WARNING, "can not automatically create Builder for a non public class: " + clazzName);
            return;
        }

        final String builderClazzName = clazzName + "Builder";
        FileObject resource = processingEnv.getFiler().createSourceFile(builderClazzName);
        StringWriter writer = new StringWriter();
        writer.write("package ");
        PackageElement packageElement = (PackageElement) clazz.getEnclosingElement();
        writer.write(packageElement.getQualifiedName().toString());
        writer.write(";\n");
        writer.write("public class ");
        writer.write(builderClazzName);
        writer.write("{\n");
        writer.write("\tpublic ");
        writer.write(builderClazzName);
        writer.write(" (){}\n");
        writer.write("\tpublic ");
        writer.write(clazzName);
        writer.write(" createInstance(){\n\t\treturn new ");
        writer.write(clazzName);
        writer.write("(");
        writer.write(");\n");
        writer.write("\t}\n");
        writer.write("}\n");
        try(Writer w = resource.openWriter()) {
            w.write(writer.toString());
        }
    }
}
